import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    videoCard:{
        marginVertical: 15
    },
    thumbnail: {
        width: '100%', 
        aspectRatio: 16/9,
    },
    timerControl: {
        backgroundColor: '#00000099',
        height: 25,
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        position: 'absolute',
        right: 5,
        bottom: 5
    },
    time: {
        color: '#ffffff',
        fontWeight: 'bold'
    },
    titleRow:{
        flexDirection: 'row',
    padding: 10,
    },
    avatar:{
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    middleContainer:{
        marginHorizontal:10,
        flex:1,
    },
    title:{
        color: '#ffffff',
        fontSize:16,
        fontWeight: '500',
        marginBottom:5
    },
    subtitle:{
        color: 'grey',
        fontSize:14,
        fontWeight: 'bold'
    }
})

export default styles;