import React, { useState } from "react";
import { View, Text, TextInput, Pressable } from "react-native";
import { BottomSheetFlatList } from "@gorhom/bottom-sheet";
import { Feather } from "@expo/vector-icons";
import VideoComment from "../VideoComment"

import comments from "../../assets/data/comments.json"
interface VideoCommentsProps {
    comments: Comment[];
    videoID: string;
  }

const VideoComments = ({ comments, videoID }: VideoCommentsProp) => {
    const [newComment, setNewComment] = useState("");

    return (
        <View style={{ backgroundColor: "#141414", flex: 1 }}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TextInput
          placeholder="what do you think?"
          value={newComment}
          onChangeText={setNewComment}
          placeholderTextColor="grey"
          style={{
            backgroundColor: "#010101",
            color: "white",
            padding: 10,
            flex: 1,
          }}
        />
        <Pressable>
          <Feather name="send" size={24} color="white" />
        </Pressable>
      </View>
      <BottomSheetFlatList
        data={comments}
        renderItem={({ item }) => <VideoComment  />}
      />

     
    </View>
    )
}

export default VideoComments


